package snep.narcisa.lab10.ex4;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class Robot implements Runnable{
    int k=0;
    String nume;

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Thread t1 = new Thread(new Robot("robot0"));
        Thread t2 = new Thread(new Robot("robot1"));
		Thread t3 = new Thread(new Robot("robot2"));
		Thread t4 = new Thread(new Robot("robot3"));
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }

    public Robot(String nume){
        this.nume = nume;
    }

    @Override
    public void run(){
        int i = ThreadLocalRandom.current().nextInt(0, 100);
        int j = ThreadLocalRandom.current().nextInt(0, 100);
        while(true){
            int randomNum = ThreadLocalRandom.current().nextInt(0, 5);
            switch (randomNum) {
                case 0://mutare in sus
                    if(i-1 > 0){
                        i=i-1;
                    }
                    break;
                case 1://mutare in jos
                    if(i+1<300){
                        i=i+1;
                    }
                    break;
                case 2://mutare in stanga
                    if(j-1 > 0){
                        j=j-1;
                    }
                    break;
                case 3://mutare in dreapta
                    if(j+1 < 300){
                        j=j+1;
                    }
                    break;
                default:
                    break;
            }
            System.out.println(nume);
            System.out.print("  pozitie: " + i +" " +j);
            System.out.println();
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


}

