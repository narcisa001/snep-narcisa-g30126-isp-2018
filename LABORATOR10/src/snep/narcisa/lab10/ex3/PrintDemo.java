package snep.narcisa.lab10.ex3;

class PrintDemo {
    public void printCount() {
        try {
            for(int i = 0; i <100; i++) {
                System.out.println("Counter   ---   "  + i );
            }
        } catch (Exception e) {
            System.out.println("Thread  interrupted.");
        }
    }
    public void printCountSecondThread() {
        try {
            for(int i = 100; i <200; i++) {
                System.out.println("Counter   ---   "  + i );
            }
        } catch (Exception e) {
            System.out.println("Thread  interrupted.");
        }
    }
}

