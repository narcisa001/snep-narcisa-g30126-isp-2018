package snep.narcisa.lab10.ex3;

import snep.narcisa.lab10.ex1.Counter;

public class Conterthread extends Thread  {

    public static void main(String args[]) {
        PrintDemo PD = new PrintDemo();

        Counter1 c1 = new Counter1( "Thread - 1 ", PD );
        Counter2 c2 = new Counter2( "Thread - 2 ", PD );

        c1.start();
        c2.start();

        // wait for threads to end
        try {
            c1.join();
            c2.join();
        } catch ( Exception e) {
            System.out.println("Interrupted");
        }
    }

    }
