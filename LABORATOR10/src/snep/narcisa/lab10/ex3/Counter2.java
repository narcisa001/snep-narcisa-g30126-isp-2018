package snep.narcisa.lab10.ex3;



public class Counter2 extends Thread {
    private Thread t;
    private String threadName;
    PrintDemo  PD;

    Counter2( String name,  PrintDemo pd) {
        threadName = name;
        PD = pd;
    }

    public void run() {
        synchronized(PD) {
            PD.printCountSecondThread();
        }
        System.out.println("Thread " +  threadName + " exiting.");
    }

    public void start () {
        System.out.println("Starting " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}
