package narcisa.snep.lab7.ex2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class IOFile {
//    public  static void main(String [] args) throws IOException {
//        Path absolutePath = Paths.get("C:\\Users\\NARCISA\\Desktop\\date.txt");
//        List<String> file = Files.readAllLines(absolutePath);
//        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(String.valueOf(absolutePath))) {
//            String fileLine;
//            int numberOfFoundLEtters=0;
//            while ((fileLine = bufferedReader.readLine()) != null) {
//                if(fileLine.contains("e")){
//                    numberOfFoundLEtters++;
//                }
//                System.out.println("Number of 'e' letters found is:"+numberOfFoundLEtters);
//            }
//        }
//
//
        public static void main(String[] args)
        throws IOException {
            int counter=0;
            // 1. Reading input by lines:
            BufferedReader in = new BufferedReader(
                    new FileReader("C:\\Users\\NARCISA\\Desktop\\date.txt"));
            String s, s2 = new String();
            while((s = in.readLine())!= null){
                s2 += s + "\n";
                if(s2.contains("e"))
                {
                    counter++;
                }

            }
            System.out.println(counter);
            System.out.println(s2);
            System.out.println("Am gasit fisier!");
            in.close();
  }
}
