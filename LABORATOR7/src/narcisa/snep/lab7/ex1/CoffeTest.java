package narcisa.snep.lab7.ex1;

public  class CoffeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        System.out.println("Number of coffes:");

        for(int i = 0;i<15;i++){
            Cofee c = null;
            try {
                c = mk.makeCofee();
            } catch (ExceptionCofeeMaker e) {
                System.out.println("Am prins exceptia!");
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                d.drinkCofee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
            }
            finally{
                System.out.println("Throw the cofee cup.\n");
            }
        }
    }
}//.class