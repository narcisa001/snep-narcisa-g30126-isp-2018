package narcisa.snep.lab7.ex1;

public class ExceptionCofeeMaker extends Exception {
    int numberOfCoffe;

    public ExceptionCofeeMaker() {
        this.numberOfCoffe = numberOfCoffe;
    }

    public ExceptionCofeeMaker(String message, int numberOfCoffe) {
        super(message);
        this.numberOfCoffe = numberOfCoffe;
    }

    public ExceptionCofeeMaker(String message, Throwable cause, int numberOfCoffe) {
        super(message, cause);
        this.numberOfCoffe = numberOfCoffe;
    }

    public ExceptionCofeeMaker(Throwable cause, int numberOfCoffe) {
        super(cause);
        this.numberOfCoffe = numberOfCoffe;
    }

    public ExceptionCofeeMaker(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, int numberOfCoffe) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.numberOfCoffe = numberOfCoffe;
    }
}
