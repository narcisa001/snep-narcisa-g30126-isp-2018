package narcisa.snep.lab3.ex4;

public class MyPoint {
    private int x;
    private int y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    public void setXY(int x,int y) {
        this.x = x;
        this.y=y;
    }

    @Override
    public String toString() {
        return "(" + x +
                ")" + y ;
    }
    public static double distance(int x,int y){
        double dist;
        if(x>y){
            dist= x-y;
        }
        else {
            dist=y-x;
        }
        return dist;
    }
    public static double distance(int x1,int x2,int y1,int y2){
        double distance=0;
       return  distance = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
    }
}
