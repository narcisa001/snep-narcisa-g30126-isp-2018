package narcisa.snep.lab9.ex2.narcisa.snep.lab9.ex3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FileReader extends JFrame {
    private JPanel panel;
    private JPanel buttonPanel;
    private JButton countButton;


    private JLabel label;
    private byte seconds = 30;

    public FileReader() {
        final int[] count = {0};
        JTextArea textArea = new JTextArea(5, 20);

        label = new JLabel("LOAD_HERE");
        JTextArea text = new JTextArea(10, 10);

        countButton = new JButton("COUNT");


        this.add(countButton);
        this.setLayout(null);

        countButton.setBounds(50, 100, 100, 30);
        this.add(label);
        label.setBounds(200, 200, 100, 30);
        countButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub

                count[0]++;
                if (count[0] < 35) {
                    label.setText(Integer.toString(count[0]));
                } else {
                    ((Timer)(e.getSource())).stop();
                }
            }

        });

        this.getContentPane().add(textArea, BorderLayout.CENTER);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }


}
