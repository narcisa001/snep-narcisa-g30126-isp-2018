package narcisa.snep.lab9.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Cronometru extends JFrame {

    private JPanel panel;
    private JPanel buttonPanel;
    private JButton countButton;


    private JLabel label;
    private byte seconds = 30;

    public Cronometru() {
        final int[] count = {0};

        label = new JLabel("LOAD_HERE");

        countButton = new JButton("COUNT");


        this.add(countButton);
        this.setLayout(null);

        countButton.setBounds(50, 100, 100, 30);
        this.add(label);
        label.setBounds(200, 200, 100, 30);
        countButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub

                count[0]++;
                if (count[0] < 35) {
                    label.setText(Integer.toString(count[0]));
                } else {
                    ((Timer)(e.getSource())).stop();
                }
            }

        });

        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }


}
