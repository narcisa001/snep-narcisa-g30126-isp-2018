package probleme_engleza;

class Calculation
{
    int fact(int n)
    {
        int result;

       if(n==1)
         return 1;

       result = fact(n-1) * n;
       return result;
    }
}

public class factorial_recursiv
{
     public static void main(String args[])
     {
       Calculation obj_one = new Calculation();

       int a = obj_one.fact(5);
       System.out.println("Factorialul numarului este : " + a);
     }
}
