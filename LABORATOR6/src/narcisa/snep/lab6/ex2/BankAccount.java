package narcisa.snep.lab6.ex2;
import java.util.Objects;

public class BankAccount {
    public String owner ;
    public double balance;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount){

    }
    public void deposit(double amount){

    }


}

